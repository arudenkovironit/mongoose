let express = require('express');
let router = express.Router();

// Для FS
// const fs = require('fs');

const User = require('../models/user.model');



router.get('/', function(req, res, next) {
    User.find({}, function(err, docs){
        if(err) return console.log(err);
        let usersArr = docs;
        res.render('users', { title: 'List of Users', userLength: usersArr.length, list: usersArr});
    });
});
router.post('/', function(req, res, next) {
    res.redirect('/users')
});

 // Через FS
/*
router.post('/', function(req, res, next) {
   xmkxmkxmkx {
        res.render(JSON.stringify(users, null, 2))
        });
       fs.readFile('./j.json', function (err, data) {
            if (err) throw err;
            let usersArr = User.find({}).then();
            res.render('users', { title: 'List of Users', userLength: usersArr.length, list: usersArr
            });
        });
    });
*/



const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({extended: false});

router.get('/addnewuser', urlencodedParser, function (req, res) {
    res.render('addnewuser', { title: 'Add New User'});
});
router.post('/addnewuser', urlencodedParser, function (req, res) {
    if(!req.body) return res.sendStatus(400);
    console.log(req.body);


    let newUserArr = {
        firstName:  `${req.body.firstName}`,
        lastName: `${req.body.lastName}`,
        email:  `${req.body.email}`,
    };

let data = new User(newUserArr);

data.save();
    res.redirect('/users')
    // Через FS
    /* fs.appendFile('./j.json',   JSON.stringify(newUserArr)    , function (err) {
         if (err) throw err;
     });
  res.end();*/
});


router.get('/:id/edit', urlencodedParser, function (req, res) {
    res.render('edituser', { title: 'Edit user '+req.params.id});
});

/*router.post('/:id/edit', urlencodedParser, function (req, res, next){

    User.findByIdAndUpdate(req.params.id, (err) => {
        if (err) return res.status(500).send(err);

    });
    res.redirect('/users')
});*/
router.post('/:id/edit', urlencodedParser, function (req, res, next){

    User.findById(req.params.id, function (err, doc) {
        if (err) {
            res.send(err)
        }
        doc.firstName =  `${req.body.firstName}`;
        doc.lastName = `${req.body.lastName}`;
        doc.email =  `${req.body.email}`;
        doc.save();
    });

    res.redirect('/users')
});



router.get('/:id/delete', function (req, res, next){

    User.findByIdAndRemove(req.params.id, (err) => {
        if (err) return res.status(500).send(err);
        res.redirect('/users')
    });

});



module.exports = router;



