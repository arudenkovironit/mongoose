// Удаление пользователя
function DeleteUser(id) {
    $.ajax({
        url: "/users/"+id,
        contentType: "application/json",
        method: "DELETE",
        success: function (user) {
            console.log(user);
            $("[name='" + user._id + "']").remove();
        }
    })
}

$("body").on("click", ".delete", function () {
    let id = $(this).data("id");
    DeleteUser(id);
});